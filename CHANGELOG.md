## [16.0.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v16.0.0...v16.0.1) (2020-05-28)


### Bug Fixes

* **azure-db:** increase GIT_DEPTH when cleanning up the db ([#276](https://github.com/SocialGouv/gitlab-ci-yml/issues/276)) ([b3dd95d](https://github.com/SocialGouv/gitlab-ci-yml/commit/b3dd95d757a264b7963d5338abf14a0eadfcf7df))
* **azure-db:** pull all branches ([#277](https://github.com/SocialGouv/gitlab-ci-yml/issues/277)) ([f2fba70](https://github.com/SocialGouv/gitlab-ci-yml/commit/f2fba70ea03a5962518e01cfcba9aaaee903d2f9))

# [16.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.9.0...v16.0.0) (2020-05-28)


### Bug Fixes

* **azure_db:** dont always extract secrets ([#274](https://github.com/SocialGouv/gitlab-ci-yml/issues/274)) ([0bee3b2](https://github.com/SocialGouv/gitlab-ci-yml/commit/0bee3b2935f561dfcf41c8450acddc7ccb376ffa))


### Features

* **deps:** update socialgouv/docker images docker tags to v1.17.1 ([#270](https://github.com/SocialGouv/gitlab-ci-yml/issues/270)) ([a73fc5e](https://github.com/SocialGouv/gitlab-ci-yml/commit/a73fc5edce40732f52bc830f23c23307aa9197f7))
* **deps:** update socialgouv/docker images docker tags to v1.18.0 ([#271](https://github.com/SocialGouv/gitlab-ci-yml/issues/271)) ([f060092](https://github.com/SocialGouv/gitlab-ci-yml/commit/f060092bd57d852bd89a49bbacf471c2f78ab552))
* **deps:** update socialgouv/docker images docker tags to v1.20.0 ([#273](https://github.com/SocialGouv/gitlab-ci-yml/issues/273)) ([a32de64](https://github.com/SocialGouv/gitlab-ci-yml/commit/a32de64ea624f05086fc405cae1233b9e7ae0cd5))
* **trivy:** use image subcommand ([#269](https://github.com/SocialGouv/gitlab-ci-yml/issues/269)) ([f59b2e6](https://github.com/SocialGouv/gitlab-ci-yml/commit/f59b2e64ef9260eb2907c28b9cf5478b2fbc8ee7))


* feat(snyk)!: remove snyk (#267) ([2f7f068](https://github.com/SocialGouv/gitlab-ci-yml/commit/2f7f068b2b2a3c1d3ee961d986110b33686e100d)), closes [#267](https://github.com/SocialGouv/gitlab-ci-yml/issues/267)


### BREAKING CHANGES

* remove the .base_snyk_scan

# [15.9.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.8.0...v15.9.0) (2020-05-27)


### Features

* **deps:** update socialgouv/docker images docker tags to v1.17.0 ([#268](https://github.com/SocialGouv/gitlab-ci-yml/issues/268)) ([2a73a14](https://github.com/SocialGouv/gitlab-ci-yml/commit/2a73a145a308c0c1ddaf914b209c32b5c4a56637))

# [15.8.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.7.0...v15.8.0) (2020-05-27)


### Features

* **autodevops:** auto cleanup azure dbs ([#265](https://github.com/SocialGouv/gitlab-ci-yml/issues/265)) ([b49bce1](https://github.com/SocialGouv/gitlab-ci-yml/commit/b49bce109f1f9092cb33f0162059ac3fee6b2b9f))
* **deps:** update snyk/snyk-cli docker tag to v1.320.4 ([#249](https://github.com/SocialGouv/gitlab-ci-yml/issues/249)) ([86df4b4](https://github.com/SocialGouv/gitlab-ci-yml/commit/86df4b4866f625a04d15ff57c9c117b6fb46e48d))
* **deps:** update snyk/snyk-cli docker tag to v1.320.5 ([#250](https://github.com/SocialGouv/gitlab-ci-yml/issues/250)) ([46e704b](https://github.com/SocialGouv/gitlab-ci-yml/commit/46e704ba58b4e2ec1205ecd2adfe5214c4af9e96))
* **deps:** update snyk/snyk-cli docker tag to v1.321.0 ([#251](https://github.com/SocialGouv/gitlab-ci-yml/issues/251)) ([3f98252](https://github.com/SocialGouv/gitlab-ci-yml/commit/3f98252a9018c428660be01cd0961071a4e7b4a0))
* **deps:** update snyk/snyk-cli docker tag to v1.322.0 ([#252](https://github.com/SocialGouv/gitlab-ci-yml/issues/252)) ([3927951](https://github.com/SocialGouv/gitlab-ci-yml/commit/3927951d7c4beb4ef9fe1b4033017ae456e8e5ee))
* **deps:** update snyk/snyk-cli docker tag to v1.323.1 ([#256](https://github.com/SocialGouv/gitlab-ci-yml/issues/256)) ([88096d7](https://github.com/SocialGouv/gitlab-ci-yml/commit/88096d7af512da33d6764dcf0ff4f19496ab519a))
* **deps:** update snyk/snyk-cli docker tag to v1.323.2 ([#257](https://github.com/SocialGouv/gitlab-ci-yml/issues/257)) ([3105682](https://github.com/SocialGouv/gitlab-ci-yml/commit/3105682c82a7574380f16ba1d4d9835eb1a3ce8a))
* **deps:** update snyk/snyk-cli docker tag to v1.324.0 ([#260](https://github.com/SocialGouv/gitlab-ci-yml/issues/260)) ([57df179](https://github.com/SocialGouv/gitlab-ci-yml/commit/57df179c2ce74d3c2f057e45e1e5e9336be6f5fc))
* **deps:** update snyk/snyk-cli docker tag to v1.325.0 ([#261](https://github.com/SocialGouv/gitlab-ci-yml/issues/261)) ([8ceec42](https://github.com/SocialGouv/gitlab-ci-yml/commit/8ceec42e264dd19faa9b8d65edd0506640d355db))
* **deps:** update snyk/snyk-cli docker tag to v1.326.0 ([#262](https://github.com/SocialGouv/gitlab-ci-yml/issues/262)) ([8e92b43](https://github.com/SocialGouv/gitlab-ci-yml/commit/8e92b4377fab6f25e0e6b1d0b79c734aa92ddc8c))
* **deps:** update snyk/snyk-cli docker tag to v1.327.1 ([#264](https://github.com/SocialGouv/gitlab-ci-yml/issues/264)) ([aa0547c](https://github.com/SocialGouv/gitlab-ci-yml/commit/aa0547cc89afdf3f26c9c4e1348296b0cfcb6f84))
* **deps:** update snyk/snyk-cli docker tag to v1.329.0 ([#266](https://github.com/SocialGouv/gitlab-ci-yml/issues/266)) ([7627896](https://github.com/SocialGouv/gitlab-ci-yml/commit/7627896f401484b9705bf55a05a4a5e25e8adb66))
* **deps:** update socialgouv/docker images docker tags to v1.16.0 ([#255](https://github.com/SocialGouv/gitlab-ci-yml/issues/255)) ([39226cf](https://github.com/SocialGouv/gitlab-ci-yml/commit/39226cf2051029258f60d439f167b9005fa5637a))

# [15.7.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.6.0...v15.7.0) (2020-05-14)


### Bug Fixes

* **autodevops:** change the default delete K8S namespace filter ([e8be537](https://github.com/SocialGouv/gitlab-ci-yml/commit/e8be537c3ec9fa5c9ff6e83b7e0db9bb06a4d36e))


### Features

* **deps:** update snyk/snyk-cli docker tag to v1.320.3 ([#248](https://github.com/SocialGouv/gitlab-ci-yml/issues/248)) ([23fd39b](https://github.com/SocialGouv/gitlab-ci-yml/commit/23fd39bf68a0e3a120c55d1f0c6c5287ae109c68))

# [15.6.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.5.0...v15.6.0) (2020-05-13)


### Features

* **deploy_app_chart:** archive the manifest ([#247](https://github.com/SocialGouv/gitlab-ci-yml/issues/247)) ([3de0b21](https://github.com/SocialGouv/gitlab-ci-yml/commit/3de0b214db903110e15ee384cc2df95b91a22f13))
* **deps:** update snyk/snyk-cli docker tag to v1.320.2 ([#246](https://github.com/SocialGouv/gitlab-ci-yml/issues/246)) ([55e5cbf](https://github.com/SocialGouv/gitlab-ci-yml/commit/55e5cbfa80b241c952d3f8ed09d47e709943a220))

# [15.5.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.4.0...v15.5.0) (2020-05-11)


### Bug Fixes

* **trivy:** default $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA ([#231](https://github.com/SocialGouv/gitlab-ci-yml/issues/231)) ([452b541](https://github.com/SocialGouv/gitlab-ci-yml/commit/452b541ee01bd69a5ab511c37b14ac50e2fd7e4a))


### Features

* **deps:** update node docker tag to v14 ([#234](https://github.com/SocialGouv/gitlab-ci-yml/issues/234)) ([9a468e2](https://github.com/SocialGouv/gitlab-ci-yml/commit/9a468e24c93320589662c31f1211d7bc0c860fdf))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/azure-db docker tag to v1.14.0 ([#225](https://github.com/SocialGouv/gitlab-ci-yml/issues/225)) ([8572e99](https://github.com/SocialGouv/gitlab-ci-yml/commit/8572e990b41a7ae9492039e09cf7113707b6a929))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/azure-db docker tag to v1.15.0 ([#235](https://github.com/SocialGouv/gitlab-ci-yml/issues/235)) ([d7b0736](https://github.com/SocialGouv/gitlab-ci-yml/commit/d7b0736eebef4c1d8a9a66fd06024793c4072660))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/git-deploy docker tag to v1.14.0 ([#226](https://github.com/SocialGouv/gitlab-ci-yml/issues/226)) ([fa4cb09](https://github.com/SocialGouv/gitlab-ci-yml/commit/fa4cb0979a93acb99c7ad49de729586fb65d71de))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/git-deploy docker tag to v1.15.0 ([#236](https://github.com/SocialGouv/gitlab-ci-yml/issues/236)) ([efa6197](https://github.com/SocialGouv/gitlab-ci-yml/commit/efa6197e8f857d550dcf931bd309867fb39099df))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/helm docker tag to v1.14.0 ([#227](https://github.com/SocialGouv/gitlab-ci-yml/issues/227)) ([ee951de](https://github.com/SocialGouv/gitlab-ci-yml/commit/ee951de95bf4c8b806e24e33eb4a9323a97d7af7))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/helm docker tag to v1.15.0 ([#237](https://github.com/SocialGouv/gitlab-ci-yml/issues/237)) ([f8d0fed](https://github.com/SocialGouv/gitlab-ci-yml/commit/f8d0fed918eae1f99fc20bf0254f7150fd945bd4))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/k8s-ns-killer docker tag to v1.14.0 ([#228](https://github.com/SocialGouv/gitlab-ci-yml/issues/228)) ([40cceaf](https://github.com/SocialGouv/gitlab-ci-yml/commit/40cceafdfef9aa58fa9ac03a9750c10e7e44b673))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/k8s-ns-killer docker tag to v1.15.0 ([#238](https://github.com/SocialGouv/gitlab-ci-yml/issues/238)) ([3b86030](https://github.com/SocialGouv/gitlab-ci-yml/commit/3b860302a1b9e71c2a6be0199d813047bbcb1b9f))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/kubectl docker tag to v1.14.0 ([#229](https://github.com/SocialGouv/gitlab-ci-yml/issues/229)) ([10032b1](https://github.com/SocialGouv/gitlab-ci-yml/commit/10032b106d569fa91bd1d4175b3c44ad339367c8))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/kubectl docker tag to v1.15.0 ([#239](https://github.com/SocialGouv/gitlab-ci-yml/issues/239)) ([3c5adaa](https://github.com/SocialGouv/gitlab-ci-yml/commit/3c5adaad3b38e21a84cef17eb979171a043835ed))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/pg-cleaner docker tag to v1.14.0 ([#230](https://github.com/SocialGouv/gitlab-ci-yml/issues/230)) ([df883db](https://github.com/SocialGouv/gitlab-ci-yml/commit/df883dbc6fe4a381f40cfbd529ff1cae3bce3ce3))
* **deps:** update snyk/snyk-cli docker tag to v1.320.0 ([#242](https://github.com/SocialGouv/gitlab-ci-yml/issues/242)) ([3b99a09](https://github.com/SocialGouv/gitlab-ci-yml/commit/3b99a0977ed68e288f9e255ea76c3b91d2ccba61))
* **deps:** update snyk/snyk-cli docker tag to v1.320.1 ([#245](https://github.com/SocialGouv/gitlab-ci-yml/issues/245)) ([4e32d7e](https://github.com/SocialGouv/gitlab-ci-yml/commit/4e32d7e3839e8db293282809f3ad3aa04794e990))
* **deps:** update socialgouv/docker images docker tags to v1.15.0 ([#243](https://github.com/SocialGouv/gitlab-ci-yml/issues/243)) ([992b886](https://github.com/SocialGouv/gitlab-ci-yml/commit/992b88696fd4af1fc8d272b84581e1e6828f2895))

# [15.4.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.3.0...v15.4.0) (2020-05-07)


### Features

* add trivy job ([#186](https://github.com/SocialGouv/gitlab-ci-yml/issues/186)) ([3dccf0b](https://github.com/SocialGouv/gitlab-ci-yml/commit/3dccf0b53a2164967aad43e93760e891330f7173))
* **autodevops:** remove env prefix in deployed helm chart app ([#222](https://github.com/SocialGouv/gitlab-ci-yml/issues/222)) ([bc2e03c](https://github.com/SocialGouv/gitlab-ci-yml/commit/bc2e03cdfa2a1cbeee1f935d1bab1d7fb215047e))
* **deps:** update curlimages/curl docker tag to v7.70.0 ([#204](https://github.com/SocialGouv/gitlab-ci-yml/issues/204)) ([def9e1b](https://github.com/SocialGouv/gitlab-ci-yml/commit/def9e1bfeb3a181514f4d66acfbc1f6eedcf4250))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/azure-db docker tag to v1.11.0 ([#188](https://github.com/SocialGouv/gitlab-ci-yml/issues/188)) ([9f3740c](https://github.com/SocialGouv/gitlab-ci-yml/commit/9f3740c2a21f7e2d3f437f2ca7384d32fdfaa28a))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/azure-db docker tag to v1.12.0 ([#194](https://github.com/SocialGouv/gitlab-ci-yml/issues/194)) ([5229102](https://github.com/SocialGouv/gitlab-ci-yml/commit/5229102c18d09e03d8e81bfd7035d20ea5ab53c6))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/azure-db docker tag to v1.13.0 ([#212](https://github.com/SocialGouv/gitlab-ci-yml/issues/212)) ([0e6499b](https://github.com/SocialGouv/gitlab-ci-yml/commit/0e6499b68e0db58489165c0de00024ce5ab3f36b))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/git-deploy docker tag to v1.11.0 ([#189](https://github.com/SocialGouv/gitlab-ci-yml/issues/189)) ([04c4abe](https://github.com/SocialGouv/gitlab-ci-yml/commit/04c4abefc28eafdd11472435a34173ff4538abe7))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/git-deploy docker tag to v1.12.0 ([#195](https://github.com/SocialGouv/gitlab-ci-yml/issues/195)) ([2952563](https://github.com/SocialGouv/gitlab-ci-yml/commit/29525637b485c2bd014ef0d3ae7bd0cdb0a5e865))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/git-deploy docker tag to v1.13.0 ([#213](https://github.com/SocialGouv/gitlab-ci-yml/issues/213)) ([4c26d85](https://github.com/SocialGouv/gitlab-ci-yml/commit/4c26d853ef0f1c71d3e1076b2798e89c629fa0d8))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/helm docker tag to v1.12.0 ([#190](https://github.com/SocialGouv/gitlab-ci-yml/issues/190)) ([c05c2aa](https://github.com/SocialGouv/gitlab-ci-yml/commit/c05c2aab91888b0c6902894e9a66677760fb1452))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/helm docker tag to v1.13.0 ([#214](https://github.com/SocialGouv/gitlab-ci-yml/issues/214)) ([bb1707b](https://github.com/SocialGouv/gitlab-ci-yml/commit/bb1707b61e88e80f34a3587f39f5b813e33e2d8b))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/k8s-ns-killer docker tag to v1.12.0 ([#192](https://github.com/SocialGouv/gitlab-ci-yml/issues/192)) ([fd031c3](https://github.com/SocialGouv/gitlab-ci-yml/commit/fd031c37ca5711581bb113626b14c9914c7350b8))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/k8s-ns-killer docker tag to v1.13.0 ([#215](https://github.com/SocialGouv/gitlab-ci-yml/issues/215)) ([9815b4d](https://github.com/SocialGouv/gitlab-ci-yml/commit/9815b4d7342f52ee3511a963700dc321700f8e74))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/kubectl docker tag to v1.12.0 ([#191](https://github.com/SocialGouv/gitlab-ci-yml/issues/191)) ([aeb2a77](https://github.com/SocialGouv/gitlab-ci-yml/commit/aeb2a777eb238398962bf97f8525e24221016f7b))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/kubectl docker tag to v1.13.0 ([#216](https://github.com/SocialGouv/gitlab-ci-yml/issues/216)) ([60bc94b](https://github.com/SocialGouv/gitlab-ci-yml/commit/60bc94bd1a7660e0ee2b6af19e6c6b4258932c18))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/pg-cleaner docker tag to v1.12.0 ([#193](https://github.com/SocialGouv/gitlab-ci-yml/issues/193)) ([53111ad](https://github.com/SocialGouv/gitlab-ci-yml/commit/53111adce70afc2a3104ff3ec75cd4a15d761822))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/pg-cleaner docker tag to v1.13.0 ([#217](https://github.com/SocialGouv/gitlab-ci-yml/issues/217)) ([8a0111d](https://github.com/SocialGouv/gitlab-ci-yml/commit/8a0111d7088d0bfc92deb3fc9372af5b017b34f4))
* **deps:** update snyk/snyk-cli docker tag to v1.307.0 ([#187](https://github.com/SocialGouv/gitlab-ci-yml/issues/187)) ([be0fd17](https://github.com/SocialGouv/gitlab-ci-yml/commit/be0fd178904444ca57d5046234e00f10d5329ff4))
* **deps:** update snyk/snyk-cli docker tag to v1.309.0 ([#196](https://github.com/SocialGouv/gitlab-ci-yml/issues/196)) ([66d655a](https://github.com/SocialGouv/gitlab-ci-yml/commit/66d655a2afa48abab792d790f6203120ef3e8444))
* **deps:** update snyk/snyk-cli docker tag to v1.310.0 ([#197](https://github.com/SocialGouv/gitlab-ci-yml/issues/197)) ([fabc5cf](https://github.com/SocialGouv/gitlab-ci-yml/commit/fabc5cf87a743585f8187572e3d52fecea5da37f))
* **deps:** update snyk/snyk-cli docker tag to v1.310.1 ([#198](https://github.com/SocialGouv/gitlab-ci-yml/issues/198)) ([d58b5b0](https://github.com/SocialGouv/gitlab-ci-yml/commit/d58b5b028241aafe987ae06096211864584d78c5))
* **deps:** update snyk/snyk-cli docker tag to v1.311.0 ([#199](https://github.com/SocialGouv/gitlab-ci-yml/issues/199)) ([720a9e7](https://github.com/SocialGouv/gitlab-ci-yml/commit/720a9e7038da19d64c58222c975dbd511ac2282a))
* **deps:** update snyk/snyk-cli docker tag to v1.312.0 ([#200](https://github.com/SocialGouv/gitlab-ci-yml/issues/200)) ([b2e4af1](https://github.com/SocialGouv/gitlab-ci-yml/commit/b2e4af1da2a11e77d95ae0aea271fd75d7ee539d))
* **deps:** update snyk/snyk-cli docker tag to v1.313.0 ([#201](https://github.com/SocialGouv/gitlab-ci-yml/issues/201)) ([80fba75](https://github.com/SocialGouv/gitlab-ci-yml/commit/80fba751675935dcdb66607b73614ab5cb66adf1))
* **deps:** update snyk/snyk-cli docker tag to v1.313.1 ([#202](https://github.com/SocialGouv/gitlab-ci-yml/issues/202)) ([c988f76](https://github.com/SocialGouv/gitlab-ci-yml/commit/c988f76745bddf7239691fcdeb98d7f6dd54760d))
* **deps:** update snyk/snyk-cli docker tag to v1.314.0 ([#203](https://github.com/SocialGouv/gitlab-ci-yml/issues/203)) ([7618a44](https://github.com/SocialGouv/gitlab-ci-yml/commit/7618a44cb72fc6e510f3d05a726733ad996dfe25))
* **deps:** update snyk/snyk-cli docker tag to v1.315.0 ([#205](https://github.com/SocialGouv/gitlab-ci-yml/issues/205)) ([711e613](https://github.com/SocialGouv/gitlab-ci-yml/commit/711e613bae0a3771afedc935376cba3417e0d528))
* **deps:** update snyk/snyk-cli docker tag to v1.315.1 ([#206](https://github.com/SocialGouv/gitlab-ci-yml/issues/206)) ([df936ad](https://github.com/SocialGouv/gitlab-ci-yml/commit/df936ad877fb507a1e9b1aadf50d351138da10a0))
* **deps:** update snyk/snyk-cli docker tag to v1.316.0 ([#207](https://github.com/SocialGouv/gitlab-ci-yml/issues/207)) ([37661c1](https://github.com/SocialGouv/gitlab-ci-yml/commit/37661c159d71480c6d0df0402cd0a02a9055cd7e))
* **deps:** update snyk/snyk-cli docker tag to v1.316.1 ([#208](https://github.com/SocialGouv/gitlab-ci-yml/issues/208)) ([cf532cc](https://github.com/SocialGouv/gitlab-ci-yml/commit/cf532cce9b8292498c9673b25a75c9eb4a35f3da))
* **deps:** update snyk/snyk-cli docker tag to v1.316.2 ([#209](https://github.com/SocialGouv/gitlab-ci-yml/issues/209)) ([cf5c8a9](https://github.com/SocialGouv/gitlab-ci-yml/commit/cf5c8a9d65b71b5e89e95f540e1ba200dd85d577))
* **deps:** update snyk/snyk-cli docker tag to v1.317.0 ([#210](https://github.com/SocialGouv/gitlab-ci-yml/issues/210)) ([ed60965](https://github.com/SocialGouv/gitlab-ci-yml/commit/ed60965559823184057047cab22dca644cccb4ab))
* **deps:** update snyk/snyk-cli docker tag to v1.318.0 ([#211](https://github.com/SocialGouv/gitlab-ci-yml/issues/211)) ([6b69c1a](https://github.com/SocialGouv/gitlab-ci-yml/commit/6b69c1a3c912923885b4c9d081310f178bb28348))
* **deps:** update snyk/snyk-cli docker tag to v1.319.0 ([#218](https://github.com/SocialGouv/gitlab-ci-yml/issues/218)) ([da56ad7](https://github.com/SocialGouv/gitlab-ci-yml/commit/da56ad7c25cceae5bb3ce227f50321fb76f26bac))
* **deps:** update snyk/snyk-cli docker tag to v1.319.1 ([#220](https://github.com/SocialGouv/gitlab-ci-yml/issues/220)) ([93e8479](https://github.com/SocialGouv/gitlab-ci-yml/commit/93e8479dddb2947d39692761c58450318303c0c1))
* **deps:** update snyk/snyk-cli docker tag to v1.319.2 ([#223](https://github.com/SocialGouv/gitlab-ci-yml/issues/223)) ([5c7b2c1](https://github.com/SocialGouv/gitlab-ci-yml/commit/5c7b2c17159453c73b8f2771aeea5560c1116535))
* **deps:** update socialgouv/docker/kubectl docker tag to v1.10.0 ([#183](https://github.com/SocialGouv/gitlab-ci-yml/issues/183)) ([dbb8bed](https://github.com/SocialGouv/gitlab-ci-yml/commit/dbb8bed7977246b77de425283d3cb08461fca1e6))
* **deps:** update socialgouv/docker/pg-cleaner docker tag to v1.10.0 ([#184](https://github.com/SocialGouv/gitlab-ci-yml/issues/184)) ([ab05f15](https://github.com/SocialGouv/gitlab-ci-yml/commit/ab05f15c6dbac13cefb77700a7d48e7e26c6cd48))


### Reverts

* remove review/ prefix on env name ([#219](https://github.com/SocialGouv/gitlab-ci-yml/issues/219)) ([fc3a853](https://github.com/SocialGouv/gitlab-ci-yml/commit/fc3a853493f79213b7181a21dbc02e8bb5ff23fb))

# [15.3.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.2.0...v15.3.0) (2020-04-20)


### Features

* **deps:** update socialgouv/docker/azure-db docker tag to v1.10.0 ([#179](https://github.com/SocialGouv/gitlab-ci-yml/issues/179)) ([392333b](https://github.com/SocialGouv/gitlab-ci-yml/commit/392333b516321f97b8ea014273f39ce4c91be996))
* **deps:** update socialgouv/docker/git-deploy docker tag to v1.10.0 ([#180](https://github.com/SocialGouv/gitlab-ci-yml/issues/180)) ([1ea7fd2](https://github.com/SocialGouv/gitlab-ci-yml/commit/1ea7fd218430546acb945687be86006ba33ce1e4))
* **deps:** update socialgouv/docker/helm docker tag to v1.10.0 ([#181](https://github.com/SocialGouv/gitlab-ci-yml/issues/181)) ([896f3a7](https://github.com/SocialGouv/gitlab-ci-yml/commit/896f3a7d99008fb20515df0422fa6f92a5c63eca))
* **deps:** update socialgouv/docker/k8s-ns-killer docker tag to v1.10.0 ([#182](https://github.com/SocialGouv/gitlab-ci-yml/issues/182)) ([eb0adaa](https://github.com/SocialGouv/gitlab-ci-yml/commit/eb0adaa77da2aef13300552f89469667593b8f53))

# [15.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.1.0...v15.2.0) (2020-04-20)


### Features

* **azure-db:** enable hstore extension ([#156](https://github.com/SocialGouv/gitlab-ci-yml/issues/156)) ([9695754](https://github.com/SocialGouv/gitlab-ci-yml/commit/9695754230606460ee4375effef2196b3ae9bbe6))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/azure-db docker tag to v1.9.0 ([#171](https://github.com/SocialGouv/gitlab-ci-yml/issues/171)) ([5bafb42](https://github.com/SocialGouv/gitlab-ci-yml/commit/5bafb428bfb1c99eba606193d10efb1fc52bb666))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/git-deploy docker tag to v1.9.0 ([#172](https://github.com/SocialGouv/gitlab-ci-yml/issues/172)) ([647eb54](https://github.com/SocialGouv/gitlab-ci-yml/commit/647eb54ce91cf06ad85a8ea3adce9053d0cade09))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/helm docker tag to v1.9.0 ([#173](https://github.com/SocialGouv/gitlab-ci-yml/issues/173)) ([72b2d60](https://github.com/SocialGouv/gitlab-ci-yml/commit/72b2d608bc7337470c28609f9d8046e727feb42d))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/k8s-ns-killer docker tag to v1.9.0 ([#174](https://github.com/SocialGouv/gitlab-ci-yml/issues/174)) ([ec64086](https://github.com/SocialGouv/gitlab-ci-yml/commit/ec640861f2b35d2a462d8f016269aae57d77c662))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/kubectl docker tag to v1.9.0 ([#176](https://github.com/SocialGouv/gitlab-ci-yml/issues/176)) ([b390ca3](https://github.com/SocialGouv/gitlab-ci-yml/commit/b390ca3cc4519a6747313febb09cf406afa3b196))
* **deps:** update registry.gitlab.factory.social.gouv.fr/socialgouv/docker/pg-cleaner docker tag to v1.9.0 ([#175](https://github.com/SocialGouv/gitlab-ci-yml/issues/175)) ([f42ef2c](https://github.com/SocialGouv/gitlab-ci-yml/commit/f42ef2c0d0820f9d2e359b2b5f5093e6cfb0c93a))
* **deps:** update snyk/snyk-cli docker tag to v1.306.0 ([#177](https://github.com/SocialGouv/gitlab-ci-yml/issues/177)) ([e347f5f](https://github.com/SocialGouv/gitlab-ci-yml/commit/e347f5fc69c004b9418e66e8a04df4e5df1e5044))

# [15.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.0.1...v15.1.0) (2020-04-16)


### Features

* **deps:** update socialgouv/docker/helm docker tag to v1.8.0 ([#167](https://github.com/SocialGouv/gitlab-ci-yml/issues/167)) ([417fa0c](https://github.com/SocialGouv/gitlab-ci-yml/commit/417fa0c466a2f305ab67a150340b5ae77b4d9f8e))
* **deps:** update socialgouv/docker/k8s-ns-killer docker tag to v1.8.0 ([#168](https://github.com/SocialGouv/gitlab-ci-yml/issues/168)) ([452fd5b](https://github.com/SocialGouv/gitlab-ci-yml/commit/452fd5b2c62db9c697de7a36dc8f9cf072506978))

## [15.0.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v15.0.0...v15.0.1) (2020-04-15)


### Bug Fixes

* **deploy_app:** helm just is already part of socialgouv/docker/helm ([a168dcd](https://github.com/SocialGouv/gitlab-ci-yml/commit/a168dcdf16fae79fedbf53bf6e87fe66e3f2741f))
* **deps:** update socialgouv/docker/kubectl docker tag to v1.5.0 ([ee13d70](https://github.com/SocialGouv/gitlab-ci-yml/commit/ee13d70fdbb9aef893e45287831f2e565cd24a69))

# [15.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v14.0.0...v15.0.0) (2020-04-14)


* feat(autodevops)!: allow existing jobs extend (#148) ([f189b17](https://github.com/SocialGouv/gitlab-ci-yml/commit/f189b17c1878ef58e34441f432e60e7f9551151f)), closes [#148](https://github.com/SocialGouv/gitlab-ci-yml/issues/148)
* feat(autodevops)!: centralize environment in autodevops file (#145) ([f9560ec](https://github.com/SocialGouv/gitlab-ci-yml/commit/f9560ec699a14da49343312b8a7929d6fc9e8538)), closes [#145](https://github.com/SocialGouv/gitlab-ci-yml/issues/145)


### BREAKING CHANGES

* **feat(autodevops)!: allow existing jobs extend**

This might be a breaking change.
* **feat(autodevops)!: centralize environment in autodevops file**

Our autodevops now drives the environment info.

```diff

Azure db stage:
  extends: .base_azure_db_stage
+  environment:
+    name: ${CI_COMMIT_REF_NAME}-dev

Drop azure db:
  extends: .base_drop_azure_db
+  environment:
+    name: ${CI_COMMIT_REF_NAME}-dev

Create namespace stage:
  extends: .base_create_namespace_stage
+  environment:
+    name: ${CI_COMMIT_REF_NAME}-dev

Delete useless k8s ns stage:
  extends: .base_delete_useless_k8s_ns_stage
+  environment:
+    name: ${CI_COMMIT_REF_NAME}-dev

Delete useless managed postgresql:
  extends: .base_delete_useless_managed_postgresql_stage
+  environment:
+    name: ${CI_COMMIT_REF_NAME}-dev
```

# [14.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v13.2.0...v14.0.0) (2020-04-09)


* fix(app)!: do not override set production values (#126) ([339d794](https://github.com/SocialGouv/gitlab-ci-yml/commit/339d79434813c7d890bb3304be60a98db639c3c8)), closes [#126](https://github.com/SocialGouv/gitlab-ci-yml/issues/126)


### BREAKING CHANGES

* **app** do not override set production values  
    As we use the production ready values in `values.socialgouv.yaml` of https://github.com/SocialGouv/helm-charts/tree/v6.0.0/charts/app, we only need to override the render when not in production

# [13.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v13.1.0...v13.2.0) (2020-04-09)


### Features

* add mattermost notification base job ([#125](https://github.com/SocialGouv/gitlab-ci-yml/issues/125)) ([a456b95](https://github.com/SocialGouv/gitlab-ci-yml/commit/a456b95151ed0046229d1dc97a6bba4bdef3ba76))
* **snyk_scan:** add snyk.io docker image scan ([#124](https://github.com/SocialGouv/gitlab-ci-yml/issues/124)) ([1fff932](https://github.com/SocialGouv/gitlab-ci-yml/commit/1fff932dad61e94e5e8e1a47d60980f59332a122))

# [13.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v13.0.0...v13.1.0) (2020-04-07)


### Features

* **azure-db:** add pgcrypto ([#119](https://github.com/SocialGouv/gitlab-ci-yml/issues/119)) ([fc07f72](https://github.com/SocialGouv/gitlab-ci-yml/commit/fc07f726e567aba69d8d28d4006d1c3735b3b05c))

# [13.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v12.0.0...v13.0.0) (2020-04-07)


### Bug Fixes

* **deploy-app:** move HELM_RENDER_ARGS at end of the render cmd ([#123](https://github.com/SocialGouv/gitlab-ci-yml/issues/123)) ([004ce05](https://github.com/SocialGouv/gitlab-ci-yml/commit/004ce0514eed42af790e636e5b0332f58b9bc676))


* feat(deploy-app)!: force set certificate from $CONTEXT (#122) ([0b402dd](https://github.com/SocialGouv/gitlab-ci-yml/commit/0b402dd30f0f88331f46187003a4d111ada0143a)), closes [#122](https://github.com/SocialGouv/gitlab-ci-yml/issues/122)


### BREAKING CHANGES

* force set certificate from $CONTEXT  
    Might break existing certificates in production !

# [12.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v11.1.0...v12.0.0) (2020-04-06)


* feat(deps)!: update socialgouv/app github release tag to v6.0.0 (#120) ([90cfd3c](https://github.com/SocialGouv/gitlab-ci-yml/commit/90cfd3c73c12fb7f5640233879a7c373a7ad3e63)), closes [#120](https://github.com/SocialGouv/gitlab-ci-yml/issues/120)


### BREAKING CHANGES

* **Update socialgouv/app github release tag to v6.0.0**

    A `PORT` variable is now required

    Before

    ```yaml
    Deploy myapp:
      extends:
        - .base_deploy_app_chart_stage
      variables:
        CONTEXT: app
        VALUES_FILE: ./.k8s/app.values.yml
    ```

    After

    ```yaml
    Deploy myapp:
      extends:
        - .base_deploy_app_chart_stage
      variables:
        PORT: 8080
        CONTEXT: app
        VALUES_FILE: ./.k8s/app.values.yml
    ```

see https://github.com/SocialGouv/helm-charts/releases/tag/v6.0.0

# [11.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v11.0.0...v11.1.0) (2020-04-04)


### Features

* add NOTIFY_DISABLED variable ([#118](https://github.com/SocialGouv/gitlab-ci-yml/issues/118)) ([35f1e98](https://github.com/SocialGouv/gitlab-ci-yml/commit/35f1e98a373333462e3f494efe79a8ad48a23dab))

# [11.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v10.0.0...v11.0.0) (2020-04-02)


* feat(autodevops)!: use .base_deploy_app_chart_stage (#117) ([05fb547](https://github.com/SocialGouv/gitlab-ci-yml/commit/05fb5475663aa6830bbcaaedb0de62c9df4db277)), closes [#117](https://github.com/SocialGouv/gitlab-ci-yml/issues/117)
* feat(deploy)!: introduce new deploy app chart (#116) ([981167a](https://github.com/SocialGouv/gitlab-ci-yml/commit/981167a98f6c6d446513fa37386799ef822ed555)), closes [#116](https://github.com/SocialGouv/gitlab-ci-yml/issues/116)


### Features

* **autodevops:** make lint and test jobs optional ([#112](https://github.com/SocialGouv/gitlab-ci-yml/issues/112)) ([c7dcc7b](https://github.com/SocialGouv/gitlab-ci-yml/commit/c7dcc7b714609d7521638cb92c10f58982c2ed7a))


### BREAKING CHANGES

* **autodevops**: use .base_deploy_app_chart_stage
    - We are now using the fusion of the hpa and nodejs chart as in deployment. This new chart can drastically reduce what you need to put in your k8s values file. see https://github.com/SocialGouv/helm-charts/tree/v5.2.2/charts/app
* introduce new deploy app chart
    - We replaced `base_deploy_hpa_chart_stage` and `base_deploy_nodejs_chart_stage` by one `base_deploy_app_chart_stage`
    that does the same as both.
    - The `base_deploy_app_chart_stage` is using the new [socialgouv/app helm chart](https://github.com/SocialGouv/helm-charts/releases/tag/v5.0.0)
    - The `base_deploy_app_chart_stage` has a new [values.socialgouv.yaml](https://github.com/SocialGouv/helm-charts/blob/v5.0.0/charts/app/values.socialgouv.yaml)
    that provides some common SocialGouv production values.
    You can override them through the `HELM_RENDER_ARGS` env variable.

# [10.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v9.1.1...v10.0.0) (2020-03-25)


### Bug Fixes

* **yarn:** remove extra stage, image ([#108](https://github.com/SocialGouv/gitlab-ci-yml/issues/108)) ([2a567f5](https://github.com/SocialGouv/gitlab-ci-yml/commit/2a567f5f1de9e8795750b76d38d8d5471873c44e))


* feat(charts)!: update to socialgouv/helm-chats@4.0.1 (#101) ([368ce63](https://github.com/SocialGouv/gitlab-ci-yml/commit/368ce6305d61b66c39363c5fcfee32126a1d31ef)), closes [#101](https://github.com/SocialGouv/gitlab-ci-yml/issues/101) 
  - see https://github.com/SocialGouv/helm-charts/blob/master/CHANGELOG.md#400-2020-03-03


### BREAKING CHANGES

* update to socialgouv/helm-chats@4.0.1

## [9.1.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v9.1.0...v9.1.1) (2020-03-10)


### Bug Fixes

* **deps:** update registry.gitlab.factory.social.gouv.fr/social… ([#94](https://github.com/SocialGouv/gitlab-ci-yml/issues/94)) ([172eed8](https://github.com/SocialGouv/gitlab-ci-yml/commit/172eed8928521d9a5a059dbbf229f5c9b36e07e1))

# [9.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v9.0.0...v9.1.0) (2020-03-10)


### Features

* **autodevops:** aggresive install-build-register speed ([#85](https://github.com/SocialGouv/gitlab-ci-yml/issues/85)) ([d6e529c](https://github.com/SocialGouv/gitlab-ci-yml/commit/d6e529c4d62cc4372ac053b8d99bafdc309a0f26))
* **autodevops:** get pg admin cred from k8s secret ([#91](https://github.com/SocialGouv/gitlab-ci-yml/issues/91)) ([d0c1cc9](https://github.com/SocialGouv/gitlab-ci-yml/commit/d0c1cc9ae1aa244266094801fe215d9501a536d7))

# [9.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v8.0.0...v9.0.0) (2020-03-09)


* refactor(autodevops)!: move manual azure db migration to a separate base ([997333b](https://github.com/SocialGouv/gitlab-ci-yml/commit/997333bab3214e70c884c49801d0a4ed4e5f9574))


### BREAKING CHANGES

* move manual azure db migration to a separate base

    You now need to include the base migrate to have the `Migrate Azure DB (dev)` set

    ```yml
    include:
      - project: SocialGouv/gitlab-ci-yml
        file: /base_migrate_azure_db.yml
        ref: v8.0.0
    ```

# [8.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.3.3...v8.0.0) (2020-03-03)


### Bug Fixes

* **autodevops:** use extends ([cab8f37](https://github.com/SocialGouv/gitlab-ci-yml/commit/cab8f377525c7e73fe6c508ac30c30d2bd27b832))


* feat(autodevops)!: add PG deployment with ENABLE_AZURE_POSTGRES (#69) ([badff1e](https://github.com/SocialGouv/gitlab-ci-yml/commit/badff1ec7b9721bdae4558e600229ed0d3cded9a)), closes [#69](https://github.com/SocialGouv/gitlab-ci-yml/issues/69)


### BREAKING CHANGES

* add PG deployment with ENABLE_AZURE_POSTGRES
    - We can now add `ENABLE_AZURE_POSTGRES` to enable postgres deployment using [azure-db](https://github.com/SocialGouv/docker/tree/master/azure-db)

## [7.3.3](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.3.2...v7.3.3) (2020-02-25)


### Bug Fixes

* **delete:** update default namespace filter ([#57](https://github.com/SocialGouv/gitlab-ci-yml/issues/57)) ([e3dfd7a](https://github.com/SocialGouv/gitlab-ci-yml/commit/e3dfd7a41d4a8d7f3d87ef48ce8ed31acd74a042))

## [7.3.2](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.3.1...v7.3.2) (2020-02-25)


### Bug Fixes

* **autodevops:** ensure that prod deploy has PRODUCTION set ([#56](https://github.com/SocialGouv/gitlab-ci-yml/issues/56)) ([6e3eb7e](https://github.com/SocialGouv/gitlab-ci-yml/commit/6e3eb7ebcc6eec81b922c8a65e73b163a94e354d))

## [7.3.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.3.0...v7.3.1) (2020-02-25)


### Bug Fixes

* **autodevops:** remove the gitlab project id on the namespace in prod ([25fb5ec](https://github.com/SocialGouv/gitlab-ci-yml/commit/25fb5ec897958a0087b813b4454a52d5d1619850))

# [7.3.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.2.0...v7.3.0) (2020-02-12)


### Features

* add postgresql cleaner stage ([#47](https://github.com/SocialGouv/gitlab-ci-yml/issues/47)) ([afc57dc](https://github.com/SocialGouv/gitlab-ci-yml/commit/afc57dc44eed1338d232d88e5f47cda73bade4b2))

# [7.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.1.0...v7.2.0) (2020-02-12)


### Features

* **hpa:** prefer using global variables ([#45](https://github.com/SocialGouv/gitlab-ci-yml/issues/45)) ([476997b](https://github.com/SocialGouv/gitlab-ci-yml/commit/476997b3c821a7336b337c66dd9be21aab3ee80d))
* **namespaces:** prefer using global variables ([#44](https://github.com/SocialGouv/gitlab-ci-yml/issues/44)) ([107165c](https://github.com/SocialGouv/gitlab-ci-yml/commit/107165c09346698657b5cee4345e1cb55b739e17))
* **namespaces:** prefer using global variables ([#46](https://github.com/SocialGouv/gitlab-ci-yml/issues/46)) ([fc460b1](https://github.com/SocialGouv/gitlab-ci-yml/commit/fc460b1e66907dda4a01f24e96f97f47ccb67f05))

# [7.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.0.1...v7.1.0) (2020-02-11)


### Features

* **nodejs:** allow user HELM_RENDER_ARGS ([#43](https://github.com/SocialGouv/gitlab-ci-yml/issues/43)) ([792131f](https://github.com/SocialGouv/gitlab-ci-yml/commit/792131f8569c1a264366cc5acd8cae9fe375561d))

## [7.0.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v7.0.0...v7.0.1) (2020-02-11)


### Bug Fixes

* **autodevops:**  do no deploy review env on master and tags ([7783410](https://github.com/SocialGouv/gitlab-ci-yml/commit/7783410364da4a2c6b5947abd94500d0cf823371))

# [7.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v6.1.0...v7.0.0) (2020-02-10)


* ci(gitlab)!: make it gitlab.com compatible (#39) ([8e91884](https://github.com/SocialGouv/gitlab-ci-yml/commit/8e9188457ba51697392e1137321e66f27361a92e)), closes [#39](https://github.com/SocialGouv/gitlab-ci-yml/issues/39)


### Reverts

* Revert "feat(helm): install helm just and socialgouv repo by default(#37)" (#40) ([78851b4](https://github.com/SocialGouv/gitlab-ci-yml/commit/78851b448877313d115b88dcd8ee41c608d5d3c4)), closes [#37](https://github.com/SocialGouv/gitlab-ci-yml/issues/37) [#40](https://github.com/SocialGouv/gitlab-ci-yml/issues/40)


### BREAKING CHANGES

* ci(gitlab)!: make it gitlab.com compatible

    - We are manly using the `environment:name` and `environment:url` to make gitlab guess the linked pod environment in the k8s cluster.
    - We recommend that review environments are prefixed `review` : `name: ${CI_COMMIT_REF_NAME}-dev`
    - You might want to prefixed `preprod` you preprod : `name: preprod/${CI_COMMIT_TAG}-dev`
    - URL are can be generated from the environment name.
    - To fully enjoy the GitLab environments link with the k8s you should additional annotations and labels to your pods

      ```yaml

      labels:
        app.kubernetes.io/part-of: sample-next-app
        owner: sample-next-app
        team: sample-next-app

      deployment:

        annotations:
          app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
          app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}

      # etc...
      ```
    - We are now running register stages on docker:19 :tada:
    - A clean up stage can be added to no-production deployment to delete linked namespace.
    - We deprecate the `K8S_NAMESPACE` env variable in favor of `KUBE_NAMESPACE` !
      `KUBE_NAMESPACE` is generated by gitlab from the `environment:name`.
    - Artifacts should last a day now ;)
    - the `RANCHER_PROJECT_ID` variable is optional

# [6.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v6.0.0...v6.1.0) (2020-01-30)


### Features

* **helm:** install helm just and socialgouv repo by default([#37](https://github.com/SocialGouv/gitlab-ci-yml/issues/37)) ([097b187](https://github.com/SocialGouv/gitlab-ci-yml/commit/097b187ce96f6fb43a807dd29dc5fb0db7a355c6))

# [6.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v5.0.0...v6.0.0) (2020-01-30)


* feat(base_register_stage)!: add `DOCKER_VERSION` default variable (#34) ([cf874c2](https://github.com/SocialGouv/gitlab-ci-yml/commit/cf874c27945184f795dc2f9825ace96949cd288a)), closes [#34](https://github.com/SocialGouv/gitlab-ci-yml/issues/34)


### BREAKING CHANGES

* feat(base_register_stage)!: add default DOCKER_VERSION value
  - might not be compatible with our current gitlab.factory.social.gouv.fr runners

# [5.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.6.1...v5.0.0) (2020-01-10)


* feat(deps)!: update SocialGouv/helm-charts to v3.0.0 (#33) ([0916b56](https://github.com/SocialGouv/gitlab-ci-yml/commit/0916b56822adcd413c3bb3612a0795dbdee1e9a6)), closes [#33](https://github.com/SocialGouv/gitlab-ci-yml/issues/33) [#32](https://github.com/SocialGouv/gitlab-ci-yml/issues/32)


### BREAKING CHANGES

* update SocialGouv/helm-charts to v3.0.0

See https://github.com/SocialGouv/helm-charts/releases/tag/v3.0.0

## [4.6.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.6.0...v4.6.1) (2020-01-10)


### Reverts

* Revert "feat: use helm chart version 2.14.0 (#32)" ([590a4ff](https://github.com/SocialGouv/gitlab-ci-yml/commit/590a4ff09983bf2fe8470ee3a114d47f04f26e2f)), closes [#32](https://github.com/SocialGouv/gitlab-ci-yml/issues/32)

# [4.6.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.5.4...v4.6.0) (2020-01-09)


### Features

* use helm chart version 2.14.0 ([#32](https://github.com/SocialGouv/gitlab-ci-yml/issues/32)) ([93609ec](https://github.com/SocialGouv/gitlab-ci-yml/commit/93609ec618f01a54ab3dff826992a37b347b8b80))

## [4.5.4](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.5.3...v4.5.4) (2019-12-31)


### Bug Fixes

* **autodevops:** wrong production var check ([#26](https://github.com/SocialGouv/gitlab-ci-yml/issues/26)) ([dbb4c47](https://github.com/SocialGouv/gitlab-ci-yml/commit/dbb4c47dcc08eb07ad27f0507b1ae7413d1b2c0a))

## [4.5.3](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.5.2...v4.5.3) (2019-12-31)


### Bug Fixes

* **autodevops_simple_app:** use full url as HOST in notify ([f6ef651](https://github.com/SocialGouv/gitlab-ci-yml/commit/f6ef6519ac2ce907bcad4de1426e9ec0ab336861))

## [4.5.2](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.5.1...v4.5.2) (2019-12-31)


### Bug Fixes

* **autodevops_simple_app:** ensure to set the HOST on notify ([49b99d5](https://github.com/SocialGouv/gitlab-ci-yml/commit/49b99d5a3b87856672395762b05f6ea623a0d5cb))

## [4.5.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.5.0...v4.5.1) (2019-12-31)


### Bug Fixes

* **nodejs:** use variable to sync helm chart version ([e7d4487](https://github.com/SocialGouv/gitlab-ci-yml/commit/e7d44879aed0cbbeae054c29a06fe0210f834949))

# [4.5.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.4.1...v4.5.0) (2019-12-31)


### Features

* **nodejs:** use helm chart version 2.13.0 ([b368202](https://github.com/SocialGouv/gitlab-ci-yml/commit/b368202553167b9757467ccea1fea104c113fdde))

## [4.4.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.4.0...v4.4.1) (2019-12-30)


### Bug Fixes

* **nodejs:** extra "l" typo ([2638243](https://github.com/SocialGouv/gitlab-ci-yml/commit/2638243facd9a1e22d1fa996e7a0d2b5a44a9d18))

# [4.4.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.3.0...v4.4.0) (2019-12-30)


### Features

* **nodejs:** use helm chart version 2.12.0 ([34bc606](https://github.com/SocialGouv/gitlab-ci-yml/commit/34bc606f433c166f6fdaa63bf692acee6bd1aefd))

# [4.3.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.2.0...v4.3.0) (2019-12-27)


### Features

* add ssl to hpa chart ([#25](https://github.com/SocialGouv/gitlab-ci-yml/issues/25)) ([343d00d](https://github.com/SocialGouv/gitlab-ci-yml/commit/343d00d20866d835fc66940e79bab3c36d42f89e))

# [4.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.1.0...v4.2.0) (2019-12-19)


### Features

* use helm chart version 2.11.0 ([#21](https://github.com/SocialGouv/gitlab-ci-yml/issues/21)) ([3ffa3bc](https://github.com/SocialGouv/gitlab-ci-yml/commit/3ffa3bca0e42c92c53cde5fa2b0e56d1554474d5))

# [4.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.0.1...v4.1.0) (2019-12-19)


### Features

* **notify:** change production ENVIRONMENT test ([7165ca5](https://github.com/SocialGouv/gitlab-ci-yml/commit/7165ca56bd253d82b085e9f1ef6941eb16a3650c))
* **notify:** rename update url to HOST ([56c418c](https://github.com/SocialGouv/gitlab-ci-yml/commit/56c418cfb985fb5c7a17155c3e6f9ef8d79d8623))
* update hpa helm version from 2.9.0 to 2.10.0 ([#20](https://github.com/SocialGouv/gitlab-ci-yml/issues/20)) ([d01cbec](https://github.com/SocialGouv/gitlab-ci-yml/commit/d01cbec58d38cbad1766da9abd38ffa0c6e89c9e))

## [4.0.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v4.0.0...v4.0.1) (2019-12-13)


### Bug Fixes

* **node_chart:** unexpected EOF while looking for matching `"' ([7298ebb](https://github.com/SocialGouv/gitlab-ci-yml/commit/7298ebbad50e330482ff81e6f2e9625f524b00cb))

# [4.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.2.0...v4.0.0) (2019-12-13)


### Features

* **nodejs_chart:** force the wildcard-crt if not in production ([#13](https://github.com/SocialGouv/gitlab-ci-yml/issues/13)) ([dedc550](https://github.com/SocialGouv/gitlab-ci-yml/commit/dedc55024e5e40f48415e820bfeffa4925a41296))


### BREAKING CHANGES

* **nodejs_chart:** feat(nodejs_chart): force the wildcard-crt if not in production
    - **.base_deploy_nodejs_chart_stage**: The job is now forcing `--set ingress.tls[0].secretName=wildcard-crt` if not in production mode.

# [3.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.1.1...v3.2.0) (2019-12-13)


### Features

* **nodejs_chart:** force the wildcard-crt if not in production ([#13](https://github.com/SocialGouv/gitlab-ci-yml/issues/13)) ([ec5a613](https://github.com/SocialGouv/gitlab-ci-yml/commit/ec5a6137f9847975c7c92c6928880e8069143699))

## [3.1.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.1.0...v3.1.1) (2019-12-13)


### Bug Fixes

* **autodevops:** change auto PRODUCTION condition ([2e9946b](https://github.com/SocialGouv/gitlab-ci-yml/commit/2e9946b96836c247910115c2f702cd002d12e544))

# [3.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.7...v3.1.0) (2019-12-12)


### Features

* **register:** improve register code ([#12](https://github.com/SocialGouv/gitlab-ci-yml/issues/12)) ([e605bba](https://github.com/SocialGouv/gitlab-ci-yml/commit/e605bbacb263eae172568f426db2a93370bf607a))

## [3.0.7](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.6...v3.0.7) (2019-12-10)


### Bug Fixes

* **autodevops:** explicit return true in PRODUCTION var ([f14a2c4](https://github.com/SocialGouv/gitlab-ci-yml/commit/f14a2c42a8e78a877d82a4c3896bc67558f98395))

## [3.0.6](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.5...v3.0.6) (2019-12-10)


### Bug Fixes

* **autodevops:** before_script should be an array of strings ([1eaa21e](https://github.com/SocialGouv/gitlab-ci-yml/commit/1eaa21ed1f624173b156a6971aeb4ae27fbcea74))

## [3.0.5](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.4...v3.0.5) (2019-12-10)


### Bug Fixes

* **autodevops:** share resolve env ([8401939](https://github.com/SocialGouv/gitlab-ci-yml/commit/8401939dfafcf3c64c93bb7face8be27b1ecbdb8))

## [3.0.4](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.3...v3.0.4) (2019-12-10)


### Bug Fixes

* **nodejs:** allow PRODUCTION value to be self defined ([84250eb](https://github.com/SocialGouv/gitlab-ci-yml/commit/84250eb259937c71390e4a4fa2b9e2bb622b304b))

## [3.0.3](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.2...v3.0.3) (2019-12-10)


### Bug Fixes

* **nodejs:** allow HOST overwrite ([1ae8f22](https://github.com/SocialGouv/gitlab-ci-yml/commit/1ae8f22e7d89837840c3677afdc5f7aa748858a7))

## [3.0.2](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.1...v3.0.2) (2019-12-10)


### Bug Fixes

* **release:** test new release process ([3973e73](https://github.com/SocialGouv/gitlab-ci-yml/commit/3973e73976d0560d19acdd358d4801d1b5366672))

## [3.0.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v3.0.0...v3.0.1) (2019-12-10)


### Bug Fixes

* test new release process ([d98640a](https://github.com/SocialGouv/gitlab-ci-yml/commit/d98640a52e418817711a23889fe885015ec49ed5))

# [3.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v2.2.1...v3.0.0) (2019-12-10)


### Features

* **autodevops:** level 1 simple apps ([#9](https://github.com/SocialGouv/gitlab-ci-yml/issues/9)) ([cfef1ee](https://github.com/SocialGouv/gitlab-ci-yml/commit/cfef1ee932f26c75ef613a1185ff5c90587ab7ac))
* **hpa:** add ability to use horizontal pod autoscaler ([#10](https://github.com/SocialGouv/gitlab-ci-yml/issues/10)) ([8fffcd8](https://github.com/SocialGouv/gitlab-ci-yml/commit/8fffcd8cd8f64294d55cce5af2b01caedc7b0e67))


### BREAKING CHANGES

* **autodevops:** feat(autodevops): level 1 simple apps
  - Might break namespace creation
  - Might break register jobs

## [2.2.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v2.2.0...v2.2.1) (2019-12-06)


### Bug Fixes

* bump to socialgouv/docker/*:0.18.0 ([1301272](https://github.com/SocialGouv/gitlab-ci-yml/commit/13012721ee3429f9464605fb36db70276308e235))

# [2.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v2.1.0...v2.2.0) (2019-11-27)


### Features

* add delete useless k8s ns stage ([1d727b7](https://github.com/SocialGouv/gitlab-ci-yml/commit/1d727b799be492d842ee0d0b717955bd26741e63))

# [2.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v2.0.0...v2.1.0) (2019-11-25)


### Features

* **nodejs_chart:** update chart to socialgouv/nodejs[#2](https://github.com/SocialGouv/gitlab-ci-yml/issues/2).8.0 ([9e6ffa3](https://github.com/SocialGouv/gitlab-ci-yml/commit/9e6ffa39454e7b1f101890fdb9e70b579e3c2f69))

# [2.0.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.5.0...v2.0.0) (2019-11-24)


### Features

* **nodejs_chart:** empty HELM_RENDER_ARGS ([e335c1c](https://github.com/SocialGouv/gitlab-ci-yml/commit/e335c1c9f59f2868e2f34730800e87bf8fa1c5e0))


### BREAKING CHANGES

* **nodejs_chart:** **feat(nodejs_chart)**: empty HELM_RENDER_ARGS
  - No args are given by default (expect the `--values`)
    We recommend that in your project, you at least set :

    ```yaml
    .deploy_job:
      extends: .base_deploy_nodejs_chart_stage
      # [...]
      before_script:
        - *resolve_env_domain
        - HOST=${FRONTEND_HOST}
        - HELM_RENDER_ARGS="
            --set image.tag=${CI_COMMIT_SHA}
            --set ingress.hosts[0].host=${HOST}
            --set ingress.tls[0].hosts[0]=${HOST}"
        - |
          if [[ "${BRANCH_NAME}" = "master" ]]; then
            HELM_RENDER_ARGS="
              ${HELM_RENDER_ARGS}
              --set ingress.annotations."certmanager\.k8s\.io/cluster-issuer"=letsencrypt-prod
              --set ingress.annotations."kubernetes\.io/tls-acme"=true
              --set ingress.tls[0].secretName=${PROJECT}-certificate"
          fi
    ```

# [1.5.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.4.0...v1.5.0) (2019-11-24)


### Features

* **nodejs_chart:** HELM_RENDER_ARGS only define image.tag by default ([130fe3e](https://github.com/SocialGouv/gitlab-ci-yml/commit/130fe3e6d39f7f47114ed48e9b99ecd331ceee09))

# [1.4.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.3.1...v1.4.0) (2019-11-22)


### Features

* **nodejs_chart:** add HELM_RENDER_ARGS var ([1cdbdfc](https://github.com/SocialGouv/gitlab-ci-yml/commit/1cdbdfc1c88c03c80db94be9af39177f5933aec8))

## [1.3.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.3.0...v1.3.1) (2019-11-20)


### Bug Fixes

* **helm:** typo in kubectl extends name ([c069be4](https://github.com/SocialGouv/gitlab-ci-yml/commit/c069be43a3d41e8bf9ca35fbe73630df4289c75d))
* **nodejs_chart:** use HOST as variable ([4a81cc5](https://github.com/SocialGouv/gitlab-ci-yml/commit/4a81cc5c81f037f9ab7aee062ec65e916f4456d4))

# [1.3.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.2.2...v1.3.0) (2019-11-20)


### Features

* **nodejs_chart:** add deploy nodejs chart stage ([9015c78](https://github.com/SocialGouv/gitlab-ci-yml/commit/9015c786d79eccc822f4c19d28e5a5a2af9660ef))

## [1.2.2](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.2.1...v1.2.2) (2019-11-20)


### Bug Fixes

* **namespaces:** kubectl extends name typo ([c260843](https://github.com/SocialGouv/gitlab-ci-yml/commit/c2608436f2129a6e27600665f59dfd762b22d73c))

## [1.2.1](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.2.0...v1.2.1) (2019-11-20)


### Bug Fixes

* **namespaces:** run as script by default ([33cb5c9](https://github.com/SocialGouv/gitlab-ci-yml/commit/33cb5c91a3ed4d9b5bdeda1eca1413f94f1e9e3a))
* **release:** typo in base_semantic_release_stage filename ([d869361](https://github.com/SocialGouv/gitlab-ci-yml/commit/d869361483bcd80af2fbf7270ba5b6d4806eed60))
* **release:** typo in file name ([0365717](https://github.com/SocialGouv/gitlab-ci-yml/commit/03657179ae8e7009f385e7694d0b81ce5dd01bfe))

# [1.2.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.1.0...v1.2.0) (2019-11-20)


### Features

* **namespace:** add create namespace stage ([726023c](https://github.com/SocialGouv/gitlab-ci-yml/commit/726023cb8be5e3057ee6e330da3ec9cdabe11691))

# [1.1.0](https://github.com/SocialGouv/gitlab-ci-yml/compare/v1.0.0...v1.1.0) (2019-11-20)


### Features

* **release:** add SEMANTIC_RELEASE_ARGS variable ([23fbb9b](https://github.com/SocialGouv/gitlab-ci-yml/commit/23fbb9b597136e4bffe3e16f22fcdf1cadff2b07))

# 1.0.0 (2019-11-20)


### Bug Fixes

* use git-deploy fixed image ([cae3f68](https://github.com/SocialGouv/gitlab-ci-yml/commit/cae3f6833ee3670245edb8cd4a250c44954cd46f))
* **github-pr:**  use DEV_ENVIRONMENT_NAME ([90b9905](https://github.com/SocialGouv/gitlab-ci-yml/commit/90b9905f7daf9064cdbe933d8b271c1c471e7dba))


### Features

* add semantic release stage ([#4](https://github.com/SocialGouv/gitlab-ci-yml/issues/4)) ([66c2c28](https://github.com/SocialGouv/gitlab-ci-yml/commit/66c2c28b3af99ae6ae04a16dff2baeb5e898ddc6))
